#include <iostream>
#include <itkImage.h>
#include "serieDICOM.h"
#include "visualizzaDICOMSerie.h"

int main() {
    // affinchè sia possa effettuare il windowing a runtime mantengo la serie con pixeltype con 16bit
    itk::Image<short, 3>::Pointer serie = itk::Image<short, 3>::New();

    int ww = 1500;
    int wl = -600;

    std::string seriesPath = "/home/rick/Documenti/magistrale/sdtra/Lab 8/anonimoComplete";
    serieDICOM(&serie, seriesPath);
    std::string nome = "Immagine assiale";
    visualizzaDICOMSerie(serie, nome);

    return 0;
}