#include "serieDICOM.h"
#include <itkImageSeriesReader.h>
#include <itkGDCMImageIO.h>
#include <itkGDCMSeriesFileNames.h>
#include <itkOrientImageFilter.h>

void serieDICOM(itk::Image<short, 3>::Pointer* p_series, std::string seriesPath)
{
    using bit16gs = short;
    using bit8gs = unsigned char;
    // name generator of files
    using NameGeneratorType = itk::GDCMSeriesFileNames;
    NameGeneratorType::Pointer nameGenerator = NameGeneratorType::New();
    nameGenerator->SetInputDirectory(seriesPath);
    nameGenerator->Update();

    // reader of 3 dimensional series of pixel type short
    using SeriesReaderType = itk::ImageSeriesReader<itk::Image<bit16gs, 3>>;
    SeriesReaderType::Pointer dicomSeriesReader = SeriesReaderType::New();
    dicomSeriesReader->SetFileNames(nameGenerator->GetFileNames(nameGenerator->GetSeriesUIDs()[0]));
    dicomSeriesReader->SetImageIO(itk::GDCMImageIO::New());
    dicomSeriesReader->Update();

    // change volume orientation
    using orienterFilter = itk::OrientImageFilter<itk::Image<bit16gs, 3>, itk::Image<bit16gs, 3>>;
    orienterFilter::Pointer orienter = orienterFilter::New();
    orienter->UseImageDirectionOn();
    orienter->SetDesiredCoordinateOrientation(itk::SpatialOrientation::ITK_COORDINATE_ORIENTATION_RAS);
    orienter->SetInput(dicomSeriesReader->GetOutput());
    orienter->Update();

    // assign to series the obtained UC series
    *p_series = orienter->GetOutput();
}