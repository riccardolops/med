#include "estraiImmagine.h"
#include <itkExtractImageFilter.h>
#include "itkOpenCVImageBridge.h"

cv::Mat estraiImmagine(int ind, itk::Image<unsigned char, 3>::Pointer series)
{
    using bitgs = unsigned char;
    itk::Image<bitgs, 3>::RegionType inputRegion = series->GetLargestPossibleRegion();
    itk::Image<bitgs, 3>::SizeType regionSize = inputRegion.GetSize();
    int numSlicesSagittal = regionSize[0];
    int numSlicesCoronal = regionSize[1];
    int numSlicesAxial = regionSize[2];

    regionSize[2] = 0;
    itk::Image<bitgs, 3>::IndexType start = inputRegion.GetIndex();
    start[2] = ind;

    itk::Image<bitgs, 3>::RegionType desiredRegion;
    desiredRegion.SetSize(regionSize);
    desiredRegion.SetIndex(start);

    using FilterType = itk::ExtractImageFilter< itk::Image<bitgs, 3>, itk::Image<bitgs, 2> >;
    FilterType::Pointer extractFilter = FilterType::New();
    extractFilter->SetExtractionRegion(desiredRegion);
    extractFilter->SetInput(series);
    extractFilter->SetDirectionCollapseToIdentity();
    extractFilter->Update();

    return itk::OpenCVImageBridge::ITKImageToCVMat<itk::Image<bitgs, 2>>(extractFilter->GetOutput());
}
