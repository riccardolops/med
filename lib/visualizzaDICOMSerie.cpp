#include "visualizzaDICOMSerie.h"
#include "opencv2/highgui.hpp"
#include "serieWindowing.h"
#include <string>

const int width_slider_max = 7000;
const int level_slider_max = 2000;

void CallBackFuncAxial(int event, int x, int y, int flags, void* userdata)
{
    std::string window_name = *((std::string**)userdata)[2];
    itk::Image<unsigned char, 3>::Pointer series = *((itk::Image<unsigned char, 3>::Pointer**)userdata)[3];
    if (strcmp(PLATFORM_NAME, "non macos") == 0) {
        // WINDOWS/LINUX SECTION
        if (event == cv::EVENT_MOUSEWHEEL)
        {
            int maxVal = *((int**)userdata)[1];
            if (cv::getMouseWheelDelta(flags) > 0)
            {
                if (*((int**)userdata)[0] < maxVal - 1)
                    *((int**)userdata)[0] += 1;
            }
            else
            {
                if (*((int**)userdata)[0] > 0)
                    *((int**)userdata)[0] -= 1;
            }
            int val = *((int**)userdata)[0];
            std::cout << "Slice: " << (val+1) << " of " << maxVal << std::endl;
            cv::Mat img = estraiImmagine(val, series);
            imshow(window_name, img);
        }
        if (event == cv::EVENT_LBUTTONDOWN)
        {
            int val = *((int**)userdata)[0];
            //seed[0] = x;
            //seed[1] = y;
            //seed[2] = val;
            return;
        }
    }
    else {
        if (event == cv::EVENT_LBUTTONDOWN && flags == cv::EVENT_FLAG_SHIFTKEY + cv::EVENT_LBUTTONDOWN)
        {
            int val = *((int**)userdata)[0];
            //seed[0] = x;
            //seed[1] = y;
            //seed[2] = val;
            return;
        }
        if (event == cv::EVENT_LBUTTONDOWN)
        {
            int maxVal = *((int**)userdata)[1];
            if (*((int**)userdata)[0] < maxVal - 1)
                *((int**)userdata)[0] += 1;
            int val = *((int**)userdata)[0];
            std::cout << "Slice: " << (val+1) << " of " << maxVal << std::endl;
            cv::Mat img = estraiImmagine(val, series);
            imshow(window_name, img);
        }
        if (event == cv::EVENT_RBUTTONDOWN)
        {
            int maxVal = *((int**)userdata)[1];
            if (*((int**)userdata)[0] > 0)
                *((int**)userdata)[0] -= 1;
            int val = *((int**)userdata)[0];
            std::cout << "Slice: " << (val+1) << " of " << maxVal << std::endl;
            cv::Mat img = estraiImmagine(val, series);
            imshow(window_name, img);
        }
    }
}

void on_buttonpress(int state, void* userdata)
{
    std::string window_name = *((std::string**)userdata)[0];
    itk::Image<short, 3>::Pointer series = *((itk::Image<short, 3>::Pointer**)userdata)[1];
    int ww = *((int**)userdata)[2];
    int wl =*((int**)userdata)[3];
    itk::Image<unsigned char, 3>::Pointer windowed = *((itk::Image<unsigned char, 3>::Pointer**)userdata)[5];
    serieWindowing(series, &windowed, wl, ww);
    *((itk::Image<unsigned char, 3>::Pointer**)userdata)[5] = windowed;
    cv::Mat img = estraiImmagine(*((int**)userdata)[4], windowed);
    imshow(window_name, img);
    cv::setTrackbarPos("Window Width", window_name, ww);
    cv::setTrackbarPos("Window Level", window_name, wl);
    
}

void on_trackbar_level(int pos, void* userdata)
{
    std::string window_name = *((std::string**)userdata)[0];
    itk::Image<short, 3>::Pointer series = *((itk::Image<short, 3>::Pointer**)userdata)[1];
    *((int**)userdata)[3] = pos;
    int ww = *((int**)userdata)[2];
    int wl =*((int**)userdata)[3];
    itk::Image<unsigned char, 3>::Pointer windowed = itk::Image<unsigned char, 3>::New();
    serieWindowing(series, &windowed, wl, ww);
    *((itk::Image<unsigned char, 3>::Pointer**)userdata)[5] = windowed;
    cv::Mat img = estraiImmagine(*((int**)userdata)[4], windowed);
    imshow(window_name, img);
}

void on_trackbar_width(int pos, void* userdata)
{
    std::string window_name = *((std::string**)userdata)[0];
    itk::Image<short, 3>::Pointer series = *((itk::Image<short, 3>::Pointer**)userdata)[1];
    *((int**)userdata)[2] = pos;
    int ww = *((int**)userdata)[2];
    int wl =*((int**)userdata)[3];
    itk::Image<unsigned char, 3>::Pointer windowed = itk::Image<unsigned char, 3>::New();
    serieWindowing(series, &windowed, wl, ww);
    *((itk::Image<unsigned char, 3>::Pointer**)userdata)[5] = windowed;

    cv::Mat img = estraiImmagine(*((int**)userdata)[4], windowed);
    imshow(window_name, img);
}

void visualizzaDICOMSerie(itk::Image<short, 3>::Pointer series, std::string window_name)
{
    int sliceNumber = 250;
    int wl = -600;
    int ww = 1500;
    itk::Image<short, 3>::RegionType inputRegion = series->GetLargestPossibleRegion();
    itk::Image<short, 3>::SizeType regionSize = inputRegion.GetSize();
    int numSlicesSagittal = regionSize[0];
    int numSlicesCoronal = regionSize[1];
    int numSlicesAxial = regionSize[2];

    itk::Image<unsigned char, 3>::Pointer windowed = itk::Image<unsigned char, 3>::New();

    serieWindowing(series, &windowed, wl, ww);

    void* userD[4] = {&sliceNumber, &numSlicesAxial, &window_name, &windowed};
    cv::Mat img = estraiImmagine(sliceNumber, windowed);
    std::string trackbar_level = "Window Level";
    std::string trackbar_width = "Window Width";
    cv::namedWindow(window_name, cv::WINDOW_NORMAL);
    imshow(window_name, img);
    void* userdata[6] = {&window_name, &series, &ww, &wl, &sliceNumber, &windowed};
    cv::createButton("Bone", on_buttonpress, userdata, cv::QT_PUSH_BUTTON);
    cv::createTrackbar(trackbar_level, window_name, NULL, level_slider_max, on_trackbar_level, userdata);
    cv::setTrackbarMin(trackbar_level,window_name, -level_slider_max);
    cv::setTrackbarPos(trackbar_level, window_name, wl);
    cv::createTrackbar(trackbar_width, window_name, NULL, width_slider_max, on_trackbar_width, userdata);
    cv::setTrackbarPos(trackbar_width, window_name, ww);
    cv::setMouseCallback(window_name, CallBackFuncAxial, userD);
    cv::waitKey();
}