#pragma once

#include <itkImage.h>
#include <opencv2/core/mat.hpp>

cv::Mat estraiImmagine(int ind, itk::Image<unsigned char, 3>::Pointer series);