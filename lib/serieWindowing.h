#pragma once

#include <itkIntensityWindowingImageFilter.h>

void serieWindowing(itk::Image<short, 3>::Pointer, itk::Image<unsigned char, 3>::Pointer*, int, int);