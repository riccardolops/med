#include "serieWindowing.h"

/* Hounsfield scale is a linear transformation of the attenuation coefficient where the radiodensity of distilled
water(STP) is 0 HU while air is -1000 HU. The window level is the "center", meaning were I'm focusing on the
spectrum. The width is a mesure of range, the narrower more contrast (good for comparing tissues of similar density),
more wide, less contrast (good for hard tissue and soft tissue). In conclusion the pixels with value below the
lower bound are all black and above higher bound are white*/

void serieWindowing(itk::Image<short, 3>::Pointer series16bit, itk::Image<unsigned char, 3>::Pointer* series8bit, int wl, int ww)
{
    // windowing the series is going from short->16bit (0-65535) to UC->8bit (0-255)
    using IntensityWindowingImageFilterType = itk::IntensityWindowingImageFilter<itk::Image<short, 3>, itk::Image<unsigned char, 3>>;
    IntensityWindowingImageFilterType::Pointer filter = IntensityWindowingImageFilterType::New();
    filter->SetInput(series16bit);
    filter->SetWindowMinimum(wl - (int)(ww / 2));
    filter->SetWindowMaximum(wl + (int)(ww / 2));
    filter->SetOutputMinimum(0);
    filter->SetOutputMaximum(pow(2, 8*sizeof(unsigned char))-1);
    filter->Update();
    *series8bit = filter->GetOutput();
}