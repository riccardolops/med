#pragma once

#include <itkImage.h>
#include "estraiImmagine.h"
#include <opencv2/highgui.hpp>

#if defined(__APPLE__) || defined(__MACH__)
#define PLATFORM_NAME "macos"
#else
#define PLATFORM_NAME "non macos"
#endif

void visualizzaDICOMSerie(itk::Image<short, 3>::Pointer, std::string);